---
---

// https://stackoverflow.com/a/7090123
const searchToObject = () => {
  var pairs = window.location.search.substring(1).split("&"),
    obj = {},
    pair,
    i;

  for (i in pairs) {
    if (pairs[i] === "") continue;

    pair = pairs[i].split("=");
    obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
  }

  return obj;
}


document.addEventListener('DOMContentLoaded', e => {
  const urlShared = searchToObject();

  document.querySelectorAll('a[lang]').forEach(a => a.href += window.location.search);

  ['t','d','u'].forEach(field => {
    if (urlShared[field] === undefined) return;

    document.querySelector(`#${field}`).innerText = urlShared[field];
  });

  document.querySelector('#text').value = Object.values(urlShared).join("\n\n");

  document.querySelector('form').addEventListener('submit', e => {
    e.target.action = document.querySelector('#instance').value + '/share';
  });
});
