# frozen_string_literal: true

# XXX: No te olvides de cambiar el nombre de archivo para que coincida
# con el nombre de la gema!
Gem::Specification.new do |spec|
  # Este es el nombre de la gema
  spec.name          = 'share-to-fediverse-jekyll-theme'
  # Versión inicial, usamos semver.org
  spec.version       = '0.1.2'
  # Una lista con todes les autores, esto se ve luego en el perfil de
  # rubygems.org
  spec.authors       = ['f']
  # Una lista de direcciones de correo, que coincide con la lista de
  # autores
  spec.email         = ['f@sutty.nl']

  # Descripción corta de la plantilla
  spec.summary       = 'A share to Fediverse microwebsite'
  # Dirección de la plantilla
  spec.homepage      = 'https://0xacab.org/sutty/jekyll/share-to-fediverse-jekyll-theme'
  # Estamos usando la licencia MIT Antifacista, que no es reconocida por
  # rubygems
  spec.license       = 'Nonstandard'

  # La gemspec por defecto incluye todos los archivos que ponemos en
  # git, pero como no todos los que pongamos son necesarios para la
  # gema, hacemos nuestra propia lista de archivos.
  spec.files         = Dir['assets/**/*',
                           '_layouts/**/*',
                           '_includes/**/*',
                           '_sass/**/*',
                           '_data/**/*',
                           '_config.yml',
                           'LICENSE*',
                           'README*']

  # Archivos que son parte de la documentación
  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  # Opciones para el generador de documentación
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  # Otros metadatos de la gema
  spec.metadata = {
    'bug_tracker_uri'   => "#{spec.homepage}/issues",
    'homepage_uri'      => spec.homepage,
    'source_code_uri'   => spec.homepage,
    'changelog_uri'     => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  # Dependencias, esto se va a instalar al instalar la gema
  spec.add_runtime_dependency 'jekyll', '~> 4.0'
  spec.add_runtime_dependency 'jekyll-relative-urls', '~> 0.0'
  spec.add_runtime_dependency 'jekyll-feed', '~> 0.9'
  spec.add_runtime_dependency 'jekyll-seo-tag', '~> 2.1'
  spec.add_runtime_dependency 'jekyll-images', '~> 0.2'
  spec.add_runtime_dependency 'jekyll-include-cache', '~> 0'
  spec.add_runtime_dependency 'jekyll-data', '~> 1.1'

  # Dependencias de desarrollo
  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'rake', '~> 12.0'
end
